Buckets are the basic containers that hold your data. Everything that you store in Cloud Storage must be contained in a bucket.<br/>
gsutil mb gs://some-bucket -- command to create a bucket<br/>
gsutil cp OBJECT_LOCATION gs://DESTINATION_BUCKET_NAME/ ++ to add an object to the bucket
