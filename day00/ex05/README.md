Environment variables are variables that define the behavior of the environment.
They can affect processes or programs that are executed in the environment.
They are accessed using $[NAME of the variable]
