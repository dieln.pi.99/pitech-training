Transport Layer Security(TLS) is a cryptographic protocol designed to secure communications of a network.
The Transport Layer Security (TLS) Handshake Protocol is responsible for the authentication and key exchange necessary to establish or resume secure sessions.
