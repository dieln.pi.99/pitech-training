Hard links - Links have actual file contents. Hard links more flexible and remain linked even if the original or linked files are moved throughout the file system
	- Created with ln [original file] [new file]
Soft Links - contains the path for original file and not the contents.
	- created with ln -s [original ] [new]
