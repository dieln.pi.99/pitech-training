def raise_to_three(src_list):
    return [x*x*x for x in src_list]
