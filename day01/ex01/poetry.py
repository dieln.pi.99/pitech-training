if __name__ == "__main__":
    poetry = """Again, I rely only on words
                    There is no music to break out of anyone's bone
                            Nor does the soul have in itself the peace proper to the happy hours 
                    Nor does the strong friend close to sad and great ideas
                And neither is an omniscient old fox"""
    print(poetry)
