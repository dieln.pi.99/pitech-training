Lists - data structure that can contain values of various types, they can contain duplicates and can be changed <br/>
Tuples - they are like lists, but cannot be changed. <br/>
Dictionaries - contain pairs of key:value elements, the elements in a dictionary are not numerically indexed, the values are accessed through the corresponding keys.
