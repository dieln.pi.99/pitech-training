Errors are the problems in a program due to which the program will stop the execution. On the other hand, exceptions are raised when the some internal events occur which changes the normal flow of the program.<br/>
Two types of Error occurs in python : Syntax errors and Logical errors (Exceptions)
