
def validate_input(inp):
    ok = 1
    if inp == " " or inp == "" or inp == "\n":
        ok = 0
    else :
        for i in inp:
            if i != " " and i not in ['0','1','2','3','4','5','6','7','8','9']:
                ok = 0
    if ok == 0:
        raise TypeError("Incorrect Type")
    return 

if __name__ == "__main__":
    inp = input()

    #remove white spaces at the beginning and at the end
    inp = inp.strip()
    
    #check type of the string
    validate_input(inp)

    #split the two numbers, remove whitespaces
    numbers = inp.split()
    a, b = int(numbers[0]), int(numbers[1])

    #check validity
    if a > b : 
        raise ValueError("Invalid Interval")
    else:
        for i in range(a, b+1):
            #print the numbers on the same line
            print(i, end=" ")
    print()
